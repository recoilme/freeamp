# WE ARE MOVED ON GITHUB!
[github.com/recoilme/freemp](https://github.com/recoilme/freemp)

# Free Android Media Player (FreeAmp)

Free Android Media Player with minimalistic interface.
Based on BASS library.

**FreeAmp can:**

- Smart organization of folders by Artists, Albums, Years & Tracknumbers.
- Not guzzle battery)
- Play music uploaded on Google Music
- Smart cover art finder

**Supported formats:**

- mp3
- flac
- ogg
- oga
- aac
- m4a
- m4b
- m4p
- opus
- wma
- wav
- mpc
- ape
- your favorite format

**Contribute:**

It is an open source player and you're welcome to contribute.

**Apk file:**

[Latest stable apk file](https://bitbucket.org/recoilme/freeamp/downloads/freeamp.apk)

**Get it on Google Play:**

[Google Play](https://play.google.com/store/apps/details?id=ru.recoilme.freeamp)

**Screenshots:**

![Screenshot](https://bitbucket.org/recoilme/freeamp/raw/master/screen1.png)![Screenshot](https://bitbucket.org/recoilme/freeamp/raw/master/screen2.png)
![Screenshot](https://bitbucket.org/recoilme/freeamp/raw/master/screen3.png)

**Troubleshooting:**

This app doesn't work / it crashed my phone / froze my system / ate my sandwich
Fill free to write me at vadim.kulibaba@gmail.com

### Author
Kulibaba Vadim <<vadim.kulibaba@gmail.com>>

### License
Distributed under [Apache 2 license](https://bitbucket.org/recoilme/freeamp/raw/master/LICENSE.txt).
